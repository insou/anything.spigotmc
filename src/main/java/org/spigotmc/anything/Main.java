package org.spigotmc.anything;

import org.bukkit.plugin.java.JavaPlugin;
import org.spigotmc.anything.fly.FlyCommand;

public class Main extends JavaPlugin {
    
    private static Main instance;

    @Override
    public void onEnable() {
        Main.instance = this;
        registerCommands();
    }

    @Override
    public void onDisable() {

    }

    public static Main getInstance (){
        return instance;
    }

    private void registerCommands() {
        getCommand("fly").setExecutor(new FlyCommand());
    }

}
