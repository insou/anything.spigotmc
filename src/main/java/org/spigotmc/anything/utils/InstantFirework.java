package org.spigotmc.anything.utils;

import java.lang.reflect.Method;

import org.bukkit.FireworkEffect;
import org.bukkit.Location;
import org.bukkit.World;
import org.bukkit.entity.Firework;
import org.bukkit.inventory.meta.FireworkMeta;

public class InstantFirework {

	private Method worldGetHandle = null;
	private Method nmsWorldBroadcastEntityEffect = null;
	private Method fireworkGetHandle = null;

	/**
	 * Play a firework effect at the location with the FireworkEffect when called
	 *
	 * @param world
	 *            The world to play the firework effect in.
	 * @param loc
	 *            The location to play the firework effect at.
	 * @param fe
	 *            The firework effect to be played.
	 * @throws Exception
	 *             This is here to stop the invoking of NMS methods from complaining
	 */
	public void playFirework(World world, Location loc, FireworkEffect fe) throws Exception {
		Firework fw = world.spawn(loc, Firework.class);
		Object nmsWorld = null;
		Object nmsFirework = null;

		if (worldGetHandle == null) {
			worldGetHandle = getMethod(world.getClass(), "getHandle");
			fireworkGetHandle = getMethod(fw.getClass(), "getHandle");
		}

		nmsWorld = worldGetHandle.invoke(world, (Object[]) null);
		nmsFirework = fireworkGetHandle.invoke(fw, (Object[]) null);

		if (nmsWorldBroadcastEntityEffect == null) {
			nmsWorldBroadcastEntityEffect = getMethod(nmsWorld.getClass(), "broadcastEntityEffect");
		}

		FireworkMeta fm = fw.getFireworkMeta();
		fm.clearEffects();
		fm.setPower(1);
		fm.addEffect(fe);
		fw.setFireworkMeta(fm);

		nmsWorldBroadcastEntityEffect.invoke(nmsWorld, new Object[] { nmsFirework, (byte) 17 });
		fw.remove();
	}

	private Method getMethod(Class<?> cl, String method) {
		for (Method m : cl.getMethods()) {
			if (m.getName().equals(method)) {
				return m;
			}
		}
		return null;
	}

}